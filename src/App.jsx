import { useState } from 'react'
import './components/Counter.css'
import './App.css'
import Counter from './components/Counter'

function App() {

  return (
    <>
      <Counter 
        name="Festival de la brique toulousaine"
      />
      <Counter 
        name="Biathlon de Noël"
      />
      <Counter 
        name="Atelier Tartiflette"
      />
      <Counter 
        name="Festival d'Arbre à Chat"
      />
    </>
  )
};

export default App