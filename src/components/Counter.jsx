import { useState } from 'react'
import ActivityName from './ActivityName';


export default function Counter(props) {
    const [counter, setCounter] = useState(0);

    // Fonction qui incrémente de 1 à chaque clic
    const add = () => {
    setCounter(counter + 1)
    };

    // Fonction qui décrémente de 1 à chaque clic sauf lorsque que le counter atteint 0
    const substract = () => {
    counter === 0 ? 0 : setCounter(counter - 1)
    };

    // Fonction qui permet de remettre le counter à 0
    const reset = () => {
    setCounter(0)
    }

  return (
    <div className="counter">
    {/* Récupération du composant ActivityName */}
      <ActivityName 
        name={props.name}
      />
      <h3>Nombre de participants : {counter}</h3>
      <span className="counter__output"></span>
      <div className="btn__container">
        <button className="button control__btn" onClick={add}>+</button>
        <button className="button control__btn" onClick={substract}>-</button>
        <button className="button reset" onClick={reset}>Reset</button>
      </div>
    </div>
  )
};